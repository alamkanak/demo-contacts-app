package com.alamkanak.codetestraquibulalam.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.alamkanak.codetestraquibulalam.model.Email;

import java.util.List;

/**
 * Created by alam on 9/26/17.
 */
@Dao
public interface EmailDao {

    @Insert
    void insertAll(List<Email> emails);

    @Query("SELECT * FROM email WHERE contactId = :contactId")
    LiveData<List<Email>> getAll(int contactId);

    @Query("DELETE FROM email WHERE contactId = :contactId")
    void deleteAll(int contactId);
}
