package com.alamkanak.codetestraquibulalam.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.alamkanak.codetestraquibulalam.model.Address;

import java.util.List;

/**
 * Created by alam on 9/26/17.
 */
@Dao
public interface AddressDao {

    @Insert
    void insertAll(List<Address> addresses);

    @Query("SELECT * FROM address WHERE contactId = :contactId")
    LiveData<List<Address>> getAll(int contactId);

    @Query("DELETE FROM address WHERE contactId = :contactId")
    void deleteAll(int contactId);
}
