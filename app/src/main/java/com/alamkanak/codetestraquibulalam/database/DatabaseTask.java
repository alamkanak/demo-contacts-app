package com.alamkanak.codetestraquibulalam.database;

import android.os.AsyncTask;

/**
 * Created by alam on 9/26/17.
 */

public class DatabaseTask extends AsyncTask<Void, Void, Void> {

    private final TaskListener listener;

    public DatabaseTask(TaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        listener.runTask();
        return null;
    }

    public interface TaskListener {
        void runTask();
    }
}
