package com.alamkanak.codetestraquibulalam.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.alamkanak.codetestraquibulalam.model.Phone;

import java.util.List;

/**
 * Created by alam on 9/26/17.
 */
@Dao
public interface PhoneDao {

    @Insert
    void insertAll(List<Phone> phones);

    @Query("SELECT * FROM phone WHERE contactId = :contactId")
    LiveData<List<Phone>> getAll(int contactId);

    @Query("DELETE FROM phone WHERE contactId = :contactId")
    void deleteAll(int contactId);
}
