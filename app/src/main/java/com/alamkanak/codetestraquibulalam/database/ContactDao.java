package com.alamkanak.codetestraquibulalam.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.alamkanak.codetestraquibulalam.model.Contact;

import java.util.List;

/**
 * Created by alam on 9/25/17.
 */
@Dao
public interface ContactDao {

    @Query("SELECT * FROM contact ORDER BY firstName, lastName")
    LiveData<List<Contact>> getAll();

    @Query("SELECT * FROM contact WHERE id = :id")
    LiveData<Contact> getById(int id);

    @Insert
    long insert(Contact contact);

    @Update
    void update(Contact contact);

    @Delete
    void delete(Contact contact);

    @Query("SELECT * FROM " +
            "(SELECT contact.*, GROUP_CONCAT(phone.phoneNumber) as phone, GROUP_CONCAT(email.emailAddress) as email FROM contact " +
            "LEFT JOIN phone ON phone.contactId = contact.id " +
            "LEFT JOIN email ON email.contactId = contact.id " +
            "GROUP BY contact.id) " +
            "WHERE firstName LIKE '%' || :query || '%' " +
            "OR lastName LIKE '%' || :query || '%' " +
            "OR phone LIKE '%' || :query || '%' " +
            "OR email LIKE '%' || :query || '%'")
    LiveData<List<Contact>> searchContacts(String query);
}
