package com.alamkanak.codetestraquibulalam.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.alamkanak.codetestraquibulalam.model.Address;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.model.Email;
import com.alamkanak.codetestraquibulalam.model.Phone;

/**
 * Created by alam on 9/25/17.
 */
@Database(entities = {Contact.class, Phone.class, Email.class, Address.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ContactDao contactDao();
    public abstract PhoneDao phoneDao();
    public abstract AddressDao addressDao();
    public abstract EmailDao emailDao();

}
