package com.alamkanak.codetestraquibulalam.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alamkanak.codetestraquibulalam.R;
import com.alamkanak.codetestraquibulalam.model.Contact;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alam on 9/25/17.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private final Context context;
    private final ItemClickListener<Contact> listener;
    private List<Contact> contacts = new ArrayList<Contact>();

    public ContactsAdapter(Context context, ItemClickListener<Contact> listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Contact contact = contacts.get(position);
        holder.textViewName.setText(context.getString(R.string.name, contact.getFirstName(), contact.getLastName()));
        holder.textViewInitial.setText(String.valueOf(contact.getFirstName().charAt(0)));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(contact);
                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return listener != null && listener.onItemLongClick(contact);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void setData(List<Contact> contacts) {
        this.contacts = contacts;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_name)
        public TextView textViewName;
        @BindView(R.id.text_view_initial)
        public TextView textViewInitial;
        public View itemView;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
