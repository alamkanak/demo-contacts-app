package com.alamkanak.codetestraquibulalam.adapter;

/**
 * Created by alam on 9/26/17.
 */

public interface ItemClickListener<T> {
    void onItemClick(T item);
    boolean onItemLongClick(T item);
}
