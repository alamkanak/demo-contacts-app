package com.alamkanak.codetestraquibulalam;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 * Created by alam on 9/27/17.
 */

public class Utils {

    /**
     * Get a readable date string with a particular format.
     * @param format Date format.
     * @param date The date to format.
     * @return Readable date string with a particular format.
     */
    public static String getReadableDate(String format, Calendar date) {
        return getReadableDate(format, date.get(Calendar.DAY_OF_MONTH), date.get(Calendar.MONTH), date.get(Calendar.YEAR));
    }

    /**
     * Get a readable date string with a particular format.
     * @param format Date format.
     * @param day Day of the month.
     * @param month Month of the year. Zero-based.
     * @param year Year.
     * @return Readable date string with a particular format.
     */
    public static String getReadableDate(String format, int day, int month, int year) {
        return String.format(format, day, new DateFormatSymbols().getMonths()[month], year);
    }
}
