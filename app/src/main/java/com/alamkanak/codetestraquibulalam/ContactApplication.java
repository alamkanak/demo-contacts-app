package com.alamkanak.codetestraquibulalam;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.alamkanak.codetestraquibulalam.database.AppDatabase;

import timber.log.Timber;

/**
 * Created by alam on 9/25/17.
 */

public class ContactApplication extends Application {

    private static AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppDatabase getDatabase() {
        if (database == null) {
            database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "contact-database").build();
        }
        return database;
    }
}
