package com.alamkanak.codetestraquibulalam.ui.edit;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alamkanak.codetestraquibulalam.R;
import com.alamkanak.codetestraquibulalam.Utils;
import com.alamkanak.codetestraquibulalam.model.Address;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.model.Email;
import com.alamkanak.codetestraquibulalam.model.Phone;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final String EXTRA_CONTACT_ID = "contact_id";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear_layout_editor)
    LinearLayout linearLayoutEditor;
    @BindView(R.id.edit_text_first_name)
    EditText editTextFirstName;
    @BindView(R.id.edit_text_last_name)
    EditText editTextLastName;
    @BindView(R.id.text_view_date_of_birth)
    TextView textViewDateOfBirth;
    @BindView(R.id.linear_layout_phone_wrapper)
    LinearLayout linearLayoutPhoneWrapper;
    @BindView(R.id.linear_layout_email_wrapper)
    LinearLayout linearLayoutEmailWrapper;
    @BindView(R.id.linear_layout_address_wrapper)
    LinearLayout linearLayoutAddressWrapper;

    String firstName;
    String lastName;
    List<String> phones = new ArrayList<String>();
    List<String> emails = new ArrayList<String>();
    List<String> addresses = new ArrayList<String>();
    private Calendar dateOfBirth;
    private EditViewModel viewModel;
    private int contactId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        viewModel = ViewModelProviders.of(this).get(EditViewModel.class);
        contactId = getIntent().getIntExtra(EXTRA_CONTACT_ID, -1);
        populateFieldsIfNecessary();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                readInputFromEditText();
                if (validateInput()) {
                    insertOrUpdateContact();
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Populate a contact so that it can be edited.
     */
    private void populateFieldsIfNecessary() {
        if (contactId < 1) {
            return;
        }
        setTitle(R.string.edit_contact);
        viewModel.getContact(contactId).observe(this, new Observer<Contact>() {
            @Override
            public void onChanged(@Nullable Contact contact) {
                if (contact != null) {
                    editTextFirstName.setText(contact.getFirstName());
                    editTextLastName.setText(contact.getLastName());
                    if (contact.getDateOfBirth() > 0) {
                        dateOfBirth = Calendar.getInstance();
                        dateOfBirth.setTimeInMillis(contact.getDateOfBirth());
                        textViewDateOfBirth.setText(Utils.getReadableDate(getString(R.string.date_format), dateOfBirth));
                    }
                }
            }
        });
        viewModel.getPhoneNumbers(contactId).observe(this, new Observer<List<Phone>>() {
            @Override
            public void onChanged(@Nullable List<Phone> phones) {
                if (phones == null || phones.size() == 0)
                    return;
                for (int i = 0; i < phones.size(); i++) {
                    String phoneNumber = phones.get(i).getPhoneNumber();
                    addEditTextAndPopulate(phoneNumber, linearLayoutPhoneWrapper, i != 0);
                }
            }
        });
        viewModel.getEmailAddresses(contactId).observe(this, new Observer<List<Email>>() {
            @Override
            public void onChanged(@Nullable List<Email> emails) {
                if (emails == null || emails.size() == 0)
                    return;
                for (int i = 0; i < emails.size(); i++) {
                    String emailAddress = emails.get(i).getEmailAddress();
                    addEditTextAndPopulate(emailAddress, linearLayoutEmailWrapper, i != 0);
                }
            }
        });
        viewModel.getAddresses(contactId).observe(this, new Observer<List<Address>>() {
            @Override
            public void onChanged(@Nullable List<Address> addresses) {
                if (addresses == null || addresses.size() == 0)
                    return;
                for (int i = 0; i < addresses.size(); i++) {
                    String address = addresses.get(i).getAddress();
                    addEditTextAndPopulate(address, linearLayoutAddressWrapper, i != 0);
                }
            }
        });
    }

    /**
     * Read input values from all edit texts.
     */
    private void readInputFromEditText() {
        firstName = editTextFirstName.getText().toString();
        lastName = editTextLastName.getText().toString();
        phones = getEditTextInputsFromLinearLayout(linearLayoutPhoneWrapper);
        emails = getEditTextInputsFromLinearLayout(linearLayoutEmailWrapper);
        addresses = getEditTextInputsFromLinearLayout(linearLayoutAddressWrapper);
    }

    /**
     * Validate inputs in all edit texts. Shows error snackbars if any input is invalid.
     * @return True if all inputs are valid.
     */
    private boolean validateInput() {
        // Check if first name and last name is provided.
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            showValidationError(R.string.name_invalid);
            return false;
        }

        // Check if at least one phone number or one email address is proivded.
        if (phones.size() == 0 && emails.size() == 0) {
            showValidationError(R.string.phone_email_invalid);
            return false;
        }

        // Validate email addresses.
        for (String email : emails) {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showValidationError(R.string.email_invalid);
                return false;
            }
        }
        return true;
    }

    /**
     * Create new contact or update existing contact in database.
     */
    private void insertOrUpdateContact() {
        if (contactId > 0) {
            viewModel.updateContact(contactId, firstName, lastName, dateOfBirth, phones, emails, addresses);
        }
        else {
            viewModel.saveContact(firstName, lastName, dateOfBirth, phones, emails, addresses);
        }
    }

    /**
     * Show an error in snackbar.
     * @param stringId Error text.
     */
    private void showValidationError(@StringRes int stringId) {
        Snackbar.make(linearLayoutEditor, getString(stringId), Snackbar.LENGTH_SHORT).show();
    }

    /**
     * Get strings written in edit texts inside a particular linear layout.
     * @param wrapper The linear layout.
     * @return List of strings inside the linear layout.
     */
    private static List<String> getEditTextInputsFromLinearLayout(LinearLayout wrapper) {
        List<String> values = new ArrayList<String>();
        for (int i = 0; i < wrapper.getChildCount(); i++) {
            View child = wrapper.getChildAt(i);
            EditText editText;
            if (child instanceof EditText) {
                editText = (EditText) child;
            }
            else {
                editText = (EditText) child.findViewById(R.id.edit_text);
            }
            if (editText != null) {
                String value = editText.getText().toString().trim();
                if (!TextUtils.isEmpty(value)) {
                    values.add(value);
                }
            }
        }
        return values;
    }

    /**
     * Click listener of date of birth button.
     */
    @OnClick(R.id.text_view_date_of_birth)
    public void onClickDateOfBirth() {
        Calendar date;
        if (dateOfBirth == null) {
            date = Calendar.getInstance();
        }
        else {
            date = (Calendar) dateOfBirth.clone();
        }
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.showYearPickerFirst(true);
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    /**
     * Click listener for "Add phone number" button.
     */
    @OnClick(R.id.button_add_phone)
    public void onClickAddPhone() {
        addNewEditText(linearLayoutPhoneWrapper, R.string.hint_phone);
    }

    /**
     * Click listener for "Add email address" button.
     */
    @OnClick(R.id.button_add_email)
    public void onClickAddEmail() {
        addNewEditText(linearLayoutEmailWrapper, R.string.hint_email);
    }

    /**
     * Click listener for "Add address" button.
     */
    @OnClick(R.id.button_add_address)
    public void onClickAddAddress() {
        addNewEditText(linearLayoutAddressWrapper, R.string.hint_address);
    }

    /**
     * Add a new edit text in a linear layout.
     * @param linearLayoutWrapper The linear layout.
     * @param hintResId Hint to show in the edit text.
     */
    private void addNewEditText(final LinearLayout linearLayoutWrapper, @StringRes int hintResId) {
        final LinearLayout editTextWrapper = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.item_edit_text, linearLayoutWrapper, false);
        ((EditText) editTextWrapper.findViewById(R.id.edit_text)).setHint(hintResId);
        editTextWrapper.findViewById(R.id.button_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayoutWrapper.removeView(editTextWrapper);
            }
        });
        linearLayoutWrapper.addView(editTextWrapper);
    }

    /**
     * Add new edit text in a linear layout and show value in it.
     * @param text Text to show in the edit text.
     * @param linearLayoutWrapper Linear layout to add edit text into.
     * @param addNewEditText True if a new edit text needs to be created. False if a edit text already exists in the linear layout.
     */
    private void addEditTextAndPopulate(String text, final LinearLayout linearLayoutWrapper, boolean addNewEditText) {
        if (!addNewEditText) {
            ((EditText) linearLayoutWrapper.getChildAt(0)).setText(text);
        }
        else {
            final LinearLayout editTextWrapper = (LinearLayout) LayoutInflater.from(EditActivity.this).inflate(R.layout.item_edit_text, linearLayoutWrapper, false);
            linearLayoutWrapper.addView(editTextWrapper);
            ((EditText) editTextWrapper.findViewById(R.id.edit_text)).setText(text);
            editTextWrapper.findViewById(R.id.button_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    linearLayoutWrapper.removeView(editTextWrapper);
                }
            });
        }
    }

    /**
     * Listener for date dialog.
     * @param view The dialog.
     * @param year Selected year.
     * @param monthOfYear Selected month.
     * @param dayOfMonth Selected day.
     */
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        dateOfBirth = Calendar.getInstance();
        dateOfBirth.set(Calendar.YEAR, year);
        dateOfBirth.set(Calendar.MONTH, monthOfYear);
        dateOfBirth.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        textViewDateOfBirth.setText(Utils.getReadableDate(getString(R.string.date_format), dayOfMonth, monthOfYear, year));
    }

    /**
     * Start EditActivity for editing a contact.
     * @param context Context that is starting the activity.
     * @param contactId Contact to edit.
     */
    public static void startActivity(Context context, int contactId) {
        Intent intent = new Intent(context, EditActivity.class);
        intent.putExtra(EXTRA_CONTACT_ID, contactId);
        context.startActivity(intent);
    }
}
