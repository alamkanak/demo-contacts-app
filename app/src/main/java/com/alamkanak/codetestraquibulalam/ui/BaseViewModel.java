package com.alamkanak.codetestraquibulalam.ui;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.alamkanak.codetestraquibulalam.ContactApplication;
import com.alamkanak.codetestraquibulalam.database.AppDatabase;

/**
 * Created by alam on 9/25/17.
 */

public abstract class BaseViewModel extends AndroidViewModel {
    public BaseViewModel(Application application) {
        super(application);
    }

    protected AppDatabase getDatabase() {
        return ((ContactApplication) getApplication()).getDatabase();
    }
}
