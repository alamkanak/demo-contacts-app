package com.alamkanak.codetestraquibulalam.ui.detail;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.alamkanak.codetestraquibulalam.R;
import com.alamkanak.codetestraquibulalam.Utils;
import com.alamkanak.codetestraquibulalam.model.Address;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.model.Email;
import com.alamkanak.codetestraquibulalam.model.Phone;
import com.alamkanak.codetestraquibulalam.ui.edit.EditActivity;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailActivity extends AppCompatActivity implements Observer<Contact> {

    public static final String EXTRA_CONTACT_ID = "contact_id";
    private int contactId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.text_view_first_name)
    TextView textViewFirstName;
    @BindView(R.id.text_view_last_name)
    TextView textViewLastName;
    @BindView(R.id.text_view_date_of_birth)
    TextView textViewDateOfBirth;
    @BindView(R.id.linear_layout_phone_wrapper)
    LinearLayout linearLayoutPhoneWrapper;
    @BindView(R.id.linear_layout_email_wrapper)
    LinearLayout linearLayoutEmailWrapper;
    @BindView(R.id.linear_layout_address_wrapper)
    LinearLayout linearLayoutAddressWrapper;
    @BindView(R.id.linear_layout_phone_root)
    LinearLayout linearLayoutPhoneRoot;
    @BindView(R.id.linear_layout_email_root)
    LinearLayout linearLayoutEmailRoot;
    @BindView(R.id.linear_layout_address_root)
    LinearLayout linearLayoutAddressRoot;
    @BindView(R.id.linear_layout_birthday_root)
    LinearLayout linearLayoutBirthdayRoot;
    @BindView(R.id.linear_layout_root)
    LinearLayout linearLayoutRoot;
    private DetailViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Read contact from database.
        contactId = getIntent().getIntExtra(EXTRA_CONTACT_ID, -1);
        if (contactId < 1) {
            finish();
        }
        viewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        viewModel.getContact(contactId).observe(this, this);

        // Populate phone numbers.
        viewModel.getPhones(contactId).observe(this, new Observer<List<Phone>>() {
            @Override
            public void onChanged(@Nullable List<Phone> phones) {
                if (phones == null || phones.size() == 0) {
                    linearLayoutPhoneRoot.setVisibility(View.GONE);
                    return;
                }
                linearLayoutPhoneRoot.setVisibility(View.VISIBLE);
                linearLayoutPhoneWrapper.removeAllViews();
                for (int i = 0; i < phones.size(); i++) {
                    final String phoneNumber = phones.get(i).getPhoneNumber();
                    inflateAndShowTextView(linearLayoutPhoneWrapper, phoneNumber, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                            startActivity(dialIntent);
                        }
                    });
                }
            }
        });

        // Populate emails.
        viewModel.getEmails(contactId).observe(this, new Observer<List<Email>>() {
            @Override
            public void onChanged(@Nullable List<Email> emails) {
                if (emails == null || emails.size() == 0) {
                    linearLayoutEmailRoot.setVisibility(View.GONE);
                    return;
                }
                linearLayoutEmailRoot.setVisibility(View.VISIBLE);
                linearLayoutEmailWrapper.removeAllViews();
                for (int i = 0; i < emails.size(); i++) {
                    final String emailAddress = emails.get(i).getEmailAddress();
                    inflateAndShowTextView(linearLayoutEmailWrapper, emailAddress, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("plain/text");
                            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
                            startActivity(Intent.createChooser(intent, "Send Email"));
                        }
                    });
                }
            }
        });

        // Populate addresses.
        viewModel.getAddresses(contactId).observe(this, new Observer<List<Address>>() {
            @Override
            public void onChanged(@Nullable List<Address> addresses) {
                if (addresses == null || addresses.size() == 0) {
                    linearLayoutAddressRoot.setVisibility(View.GONE);
                    return;
                }
                linearLayoutAddressRoot.setVisibility(View.VISIBLE);
                linearLayoutAddressWrapper.removeAllViews();
                for (int i = 0; i < addresses.size(); i++) {
                    final String address = addresses.get(i).getAddress();
                    inflateAndShowTextView(linearLayoutAddressWrapper, address, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            try {
                                startActivity(mapIntent);
                            }
                            catch (ActivityNotFoundException e) {
                                Snackbar.make(linearLayoutRoot, getString(R.string.location_error), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_delete:
                new MaterialDialog.Builder(this)
                        .content(R.string.delete_message)
                        .positiveText(R.string.delete)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Contact contact = new Contact();
                                contact.setId(contactId);
                                viewModel.delete(contact);
                                finish();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onChanged(@Nullable Contact contact) {
        if (contact == null) {
            finish();
            return;
        }
        String title = getString(R.string.name, contact.getFirstName(), contact.getLastName());
        toolbarLayout.setTitleEnabled(true);
        toolbarLayout.setTitle(title);

        // Show name.
        textViewFirstName.setText(contact.getFirstName());
        textViewLastName.setText(contact.getLastName());

        // Show birthday.
        if (contact.getDateOfBirth() > 0) {
            linearLayoutBirthdayRoot.setVisibility(View.VISIBLE);
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(contact.getDateOfBirth());
            textViewDateOfBirth.setText(Utils.getReadableDate(getString(R.string.date_format), date));
        }
        else {
            linearLayoutBirthdayRoot.setVisibility(View.GONE);
        }

    }

    /**
     * Click listener for edit button.
     */
    @OnClick(R.id.fab) @SuppressWarnings("unused")
    public void onClickEdit() {
        EditActivity.startActivity(this, contactId);
    }

    /**
     * Inflate a text view and populate a value in it.
     * @param wrapperLayout Parent layout for the text view.
     * @param text Text to show in the text view.
     * @param listener Click listener for the text view.
     */
    private void inflateAndShowTextView(LinearLayout wrapperLayout, String text, View.OnClickListener listener) {
        TextView textView = (TextView) LayoutInflater.from(DetailActivity.this).inflate(R.layout.item_text_view, wrapperLayout, false);
        textView.setText(text);
        textView.setOnClickListener(listener);
        wrapperLayout.addView(textView);
    }

    /**
     * Start DetailActivity with a particular contact.
     * @param context Context that is starting the activity.
     * @param contactId ID of the contact to show in the UI.
     */
    public static void startActivity(Context context, int contactId) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_CONTACT_ID, contactId);
        context.startActivity(intent);
    }
}
