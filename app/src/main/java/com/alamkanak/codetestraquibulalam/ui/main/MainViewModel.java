package com.alamkanak.codetestraquibulalam.ui.main;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.alamkanak.codetestraquibulalam.database.DatabaseTask;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.ui.BaseViewModel;

import java.util.List;

/**
 * Created by alam on 9/25/17.
 */

public class MainViewModel extends BaseViewModel {
    public MainViewModel(Application application) {
        super(application);
    }

    /**
     * Get all contacts from database.
     * @return All contacts from database.
     */
    LiveData<List<Contact>> getContacts() {
        return getDatabase().contactDao().getAll();
    }

    /**
     * Find contacts in database based on a query string.
     * @param query Query string.
     * @return Search result as live data.
     */
    LiveData<List<Contact>> searchContacts(String query) {
        return getDatabase().contactDao().searchContacts(query);
    }

    /**
     * Delete a particular contact.
     * @param contact Contact to delete.
     */
    void delete(final Contact contact) {
        new DatabaseTask(new DatabaseTask.TaskListener() {
            @Override
            public void runTask() {
                getDatabase().contactDao().delete(contact);
            }
        }).execute();
    }
}
