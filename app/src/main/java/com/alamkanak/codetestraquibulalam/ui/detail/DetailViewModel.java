package com.alamkanak.codetestraquibulalam.ui.detail;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.alamkanak.codetestraquibulalam.database.DatabaseTask;
import com.alamkanak.codetestraquibulalam.model.Address;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.model.Email;
import com.alamkanak.codetestraquibulalam.model.Phone;
import com.alamkanak.codetestraquibulalam.ui.BaseViewModel;

import java.util.List;

/**
 * Created by alam on 9/26/17.
 */

public class DetailViewModel extends BaseViewModel {
    public DetailViewModel(Application application) {
        super(application);
    }

    /**
     * Get a contact from database.
     * @param contactId ID of the contact.
     * @return Contact live data.
     */
    LiveData<Contact> getContact(int contactId) {
        return getDatabase().contactDao().getById(contactId);
    }

    /**
     * Get all phone numbers of a particular contact.
     * @param contactId ID of the contact.
     * @return All phone numbers contact as live data.
     */
    LiveData<List<Phone>> getPhones(int contactId) {
        return getDatabase().phoneDao().getAll(contactId);
    }

    /**
     * Get all emails of a particular contact.
     * @param contactId ID of the contact.
     * @return All emails of contact as live data.
     */
    LiveData<List<Email>> getEmails(int contactId) {
        return getDatabase().emailDao().getAll(contactId);
    }

    /**
     * Get all addresses of a particular contact.
     * @param contactId ID of the contact.
     * @return All addresses of contact as live data.
     */
    LiveData<List<Address>> getAddresses(int contactId) {
        return getDatabase().addressDao().getAll(contactId);
    }

    /**
     * Delete a particular contact.
     * @param contact Contact to delete.
     */
    void delete(final Contact contact) {
        new DatabaseTask(new DatabaseTask.TaskListener() {
            @Override
            public void runTask() {
                getDatabase().contactDao().delete(contact);
            }
        }).execute();
    }
}
