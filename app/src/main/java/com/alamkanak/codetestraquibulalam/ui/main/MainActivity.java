package com.alamkanak.codetestraquibulalam.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alamkanak.codetestraquibulalam.R;
import com.alamkanak.codetestraquibulalam.adapter.ContactsAdapter;
import com.alamkanak.codetestraquibulalam.adapter.ItemClickListener;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.ui.detail.DetailActivity;
import com.alamkanak.codetestraquibulalam.ui.edit.EditActivity;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements Observer<List<Contact>>,ItemClickListener<Contact>, MaterialSearchView.OnQueryTextListener, MaterialSearchView.SearchViewListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.search_view) MaterialSearchView searchView;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton buttonAdd;
    @BindView(R.id.text_view_empty) TextView textViewEmpty;
    private ContactsAdapter adapter;
    private boolean searchShown = false;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.getContacts().observe(this, this);
        setupRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        searchView.setOnQueryTextListener(this);
        searchView.setOnSearchViewListener(this);
        return true;
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ContactsAdapter(this, this);
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.fab) @SuppressWarnings("unused")
    public void onCreateContact() {
        startActivity(new Intent(this, EditActivity.class));
    }

    @Override
    public void onChanged(@Nullable List<Contact> contacts) {
        if (!searchShown) {
            adapter.setData(contacts);
            adapter.notifyDataSetChanged();
            if (contacts == null || contacts.size() == 0) {
                showEmptyView(R.string.hint_add);
            }
            else {
                hideEmptyView();
            }
        }
    }

    private void hideEmptyView() {
        recyclerView.setVisibility(View.VISIBLE);
        textViewEmpty.setVisibility(View.GONE);
    }

    private void showEmptyView(@StringRes int hintId) {
        textViewEmpty.setText(hintId);
        recyclerView.setVisibility(View.GONE);
        textViewEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(Contact item) {
        DetailActivity.startActivity(MainActivity.this, item.getId());
    }

    @Override
    public boolean onItemLongClick(final Contact item) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.name, item.getFirstName(), item.getLastName()))
                .items(R.array.items)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                EditActivity.startActivity(MainActivity.this, item.getId());
                                break;
                            case 1:
                                viewModel.delete(item);
                                Snackbar.make(recyclerView, R.string.deleted, Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                })
                .show();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        viewModel.searchContacts(newText).observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable List<Contact> contacts) {
                adapter.setData(contacts);
                adapter.notifyDataSetChanged();
                if (contacts == null || contacts.size() == 0) {
                    showEmptyView(R.string.hint_search);
                }
                else {
                    hideEmptyView();
                }
            }
        });
        return true;
    }

    @Override
    public void onSearchViewShown() {
        searchShown = true;
        buttonAdd.setVisibility(View.GONE);
        showEmptyView(R.string.hint_search);
    }

    @Override
    public void onSearchViewClosed() {
        searchShown = false;
        viewModel.getContacts().observe(this, this);
        buttonAdd.setVisibility(View.VISIBLE);
    }
}
