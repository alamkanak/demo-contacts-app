package com.alamkanak.codetestraquibulalam.ui.edit;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.Nullable;

import com.alamkanak.codetestraquibulalam.database.DatabaseTask;
import com.alamkanak.codetestraquibulalam.model.Address;
import com.alamkanak.codetestraquibulalam.model.Contact;
import com.alamkanak.codetestraquibulalam.model.Email;
import com.alamkanak.codetestraquibulalam.model.Phone;
import com.alamkanak.codetestraquibulalam.ui.BaseViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by alam on 9/26/17.
 */

public class EditViewModel extends BaseViewModel {

    public EditViewModel(Application application) {
        super(application);
    }

    /**
     * Get a contact from database.
     * @param contactId ID of the contact.
     * @return Contact live data.
     */
    LiveData<Contact> getContact(int contactId) {
        return getDatabase().contactDao().getById(contactId);
    }

    /**
     * Get all phone numbers of a particular contact.
     * @param contactId ID of the contact.
     * @return All phone numbers contact as live data.
     */
    LiveData<List<Phone>> getPhoneNumbers(int contactId) {
        return getDatabase().phoneDao().getAll(contactId);
    }

    /**
     * Get all emails of a particular contact.
     * @param contactId ID of the contact.
     * @return All emails of contact as live data.
     */
    LiveData<List<Email>> getEmailAddresses(int contactId) {
        return getDatabase().emailDao().getAll(contactId);
    }

    /**
     * Get all addresses of a particular contact.
     * @param contactId ID of the contact.
     * @return All addresses of contact as live data.
     */
    LiveData<List<Address>> getAddresses(int contactId) {
        return getDatabase().addressDao().getAll(contactId);
    }

    /**
     * Insert a new contact along with its phone, email and addresses in database.
     * @param firstName First name of the contact.
     * @param lastName Last name of the contact.
     * @param dateOfBirth Date of birth of the contact.
     * @param phoneNumbers Phone numbers of the contact.
     * @param emailAddresses Email addresses of the contact.
     * @param addresses Addresses of the contact.
     */
    void saveContact(final String firstName, final String lastName, @Nullable final Calendar dateOfBirth, final List<String> phoneNumbers, final List<String> emailAddresses, final List<String> addresses) {
        new DatabaseTask(new DatabaseTask.TaskListener() {
            @Override
            public void runTask() {
                // Insert contact in the database.
                Contact contact = new Contact();
                contact.setFirstName(firstName);
                contact.setLastName(lastName);
                if (dateOfBirth != null) {
                    contact.setDateOfBirth(dateOfBirth.getTimeInMillis());
                }
                long insertId = getDatabase().contactDao().insert(contact);

                // Insert all phone numbers.
                List<Phone> phones = new ArrayList<Phone>();
                for (String number : phoneNumbers) {
                    Phone phone = new Phone();
                    phone.setContactId((int) insertId);
                    phone.setPhoneNumber(number);
                    phones.add(phone);
                }
                getDatabase().phoneDao().insertAll(phones);

                // Insert all emails.
                List<Email> emails = new ArrayList<Email>();
                for (String address : emailAddresses) {
                    Email email = new Email();
                    email.setContactId((int) insertId);
                    email.setEmailAddress(address);
                    emails.add(email);
                }
                getDatabase().emailDao().insertAll(emails);

                // Insert all addresses.
                List<Address> addressList = new ArrayList<Address>();
                for (String addressLine : addresses) {
                    Address address = new Address();
                    address.setContactId((int) insertId);
                    address.setAddress(addressLine);
                    addressList.add(address);
                }
                getDatabase().addressDao().insertAll(addressList);
            }
        }).execute();
    }

    /**
     * Update an existing contact and its phones, emails and addresses.
     * @param contactId The ID of the contact to update.
     * @param firstName First name of the contact.
     * @param lastName Last name of the contact.
     * @param dateOfBirth Date of birth of the contact.
     * @param phoneNumbers Phone numbers of the contact.
     * @param emailAddresses Email addresses of the contact.
     * @param addresses Addresses of the contact.
     */
    void updateContact(final int contactId, final String firstName, final String lastName, final Calendar dateOfBirth, final List<String> phoneNumbers, final List<String> emailAddresses, final List<String> addresses) {
        new DatabaseTask(new DatabaseTask.TaskListener() {
            @Override
            public void runTask() {
                // Update the contact.
                Contact contact = new Contact();
                contact.setFirstName(firstName);
                contact.setLastName(lastName);
                contact.setId(contactId);
                if (dateOfBirth != null) {
                    contact.setDateOfBirth(dateOfBirth.getTimeInMillis());
                }
                getDatabase().contactDao().update(contact);

                // Update phone numbers.
                List<Phone> phones = new ArrayList<Phone>();
                for (String number : phoneNumbers) {
                    Phone phone = new Phone();
                    phone.setContactId((int) contactId);
                    phone.setPhoneNumber(number);
                    phones.add(phone);
                }
                getDatabase().phoneDao().deleteAll(contactId);
                getDatabase().phoneDao().insertAll(phones);

                // Update emails.
                List<Email> emails = new ArrayList<Email>();
                for (String address : emailAddresses) {
                    Email email = new Email();
                    email.setContactId((int) contactId);
                    email.setEmailAddress(address);
                    emails.add(email);
                }
                getDatabase().emailDao().deleteAll(contactId);
                getDatabase().emailDao().insertAll(emails);

                // Update addresses.
                List<Address> addressList = new ArrayList<Address>();
                for (String addressLine : addresses) {
                    Address address = new Address();
                    address.setContactId((int) contactId);
                    address.setAddress(addressLine);
                    addressList.add(address);
                }
                getDatabase().addressDao().deleteAll(contactId);
                getDatabase().addressDao().insertAll(addressList);
            }
        }).execute();
    }
}
